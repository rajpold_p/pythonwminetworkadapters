import wmi
import click


@click.group()
def main():
    """Simple program to configure active network adapters \n
        Works only on WINDOWS!!!"""
    pass


@main.command()
def simple_view():
    """Adapters simple view"""

    print("Active adapters: ")
    for i in nic_configs:
        print("-" * 50)
        print("Name: ", i.Description)
        print("IPv6 address: ", i.IPAddress[1])
        print("MAC address: ", i.MACAddress)


@main.command()
def complex_view():
    """Adapters complex view"""

    for i in nic_configs:
        print(i)


@main.command()
@click.option("--adapter", "-a", default=0, help="Index of adapter")
@click.option("--ip", help="IPv4 address")
@click.option("--mask", help="Subnetmask")
@click.option("--gateway", help="Default gateway")
def set(adapter, ip, mask, gateway):
    """Set IP address, subnetmask and default gateway"""

    nic = nic_configs[adapter]
    nic.EnableStatic(IPAddress=[ip], SubnetMask=[mask])
    nic.SetGateways(DefaultIPGateway=[gateway])


@main.command()
@click.option("--adapter", "-a", default=0, help="Index of adapter")
def set_to_dhcp(adapter):
    """Make adpater use dynamic ip address"""
    nic = nic_configs[adapter]
    nic.EnableDHCP()


if __name__ == "__main__":
    nic_configs = wmi.WMI().Win32_NetworkAdapterConfiguration(IPEnabled=True)
    main()
