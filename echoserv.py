import socket

buflen = 1024
print("---Simple TCP echo server---")
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind(('127.0.0.1', 33333))
s.listen(5)
print("Server is listening...")
clientsocket, address = s.accept()

print("Conection accepted from", address[0], " on port ", address[1])

while True:
    try:
        msgrecv = clientsocket.recv(buflen)
        msgsend = "\n" + msgrecv.decode('UTF-8') + "\n"
        clientsocket.send(msgsend.encode('UTF-8'))
    except:
        s.close()
        clientsocket.close()
        print("Server is closed")
        break
