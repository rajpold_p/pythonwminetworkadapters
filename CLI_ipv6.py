import click
import socket


@click.command()
@click.option("--port", "-p", default=1111, help="Port number")
@click.option("--addr", "-a", default='::1', help="Address")
@click.option("--buflen", "-b", default=1024, help="Length of buffer")
def tcp_echo_serv(port, addr, buflen):
    """Simple program that allows to create simple TCP echo serv"""

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    s.bind((addr, port))
    s.listen(5)
    print("Server is listening...")
    clientsocket, address = s.accept()

    print("Conection accepted from", address[0], " on port ", address[1])

    while True:
        try:
            msgrecv = clientsocket.recv(buflen)
            msgsend = "\n" + msgrecv.decode('UTF-8') + "\n"
            clientsocket.send(msgsend.encode('UTF-8'))
        except:
            s.close()
            clientsocket.close()
            print("Server is closed")
            break


if __name__ == '__main__':
    tcp_echo_serv()
